from __future__ import absolute_import

from django.conf.urls import patterns, include, url

from . import views
from .views import TalkListUpdateView, TalkListDeleteView

talks_patterns = patterns(
        '',
        # url(r'^d/(?P<slug>[-\w]+)/$', views.TalkDetailView.as_view(), name='detail'),
        url(r'^d/(?P<slug>[-\w]+)/$', views.talk_detail_view, name='detail')
)


list_patterns = patterns('',
        url(r'^$', views.talklist_list_view, name='list'),
        # url(r'^$', views.TalkListListView.as_view(), name='list'),

        url(r'^d/(?P<slug>[-\w]+)/$', views.talklist_detail_view, name='detail'),
        # url(r'^d/(?P<slug>[-\w]+)/$', views.TalkListDetailView.as_view(), name='detail'),

        # url(r'^create/$', views.TalkListCreateView.as_view(), name='create'),
        url(r'^create/$', views.talklist_create_view, name='create'),
        #
        # url(r'^e/(?P<slug>[-\w]+)/$', TalkListUpdateView.as_view(), name='update'),
        url(r'^e/(?P<slug>[-\w]+)/$', views.talklist_update_view, name='update'),

        url(r'^delete/(?P<pk>[-\w]+)/$', views.talklist_delete_view, name='delete'),
        # url(r'^delete/(?P<pk>[-\w]+)/$', TalkListDeleteView.as_view(), name='delete'),

        # url(r'^remove/(?P<talklist_pk>\d+)/(?P<pk>\d+)/$', views.TalkListRemoveTalkView.as_view(), name='remove_talk'),
        url(r'^remove/(?P<talklist_pk>\d+)/(?P<pk>\d+)/$', views.talklist_remove_talk, name='remove_talk'),
        #
        # url(r'^s/(?P<slug>[-\w]+)/$', views.TalkListScheduleView.as_view(), name='schedule'),
        url(r'^s/(?P<slug>[-\w]+)/$', views.talklist_schedule_view, name='schedule'),

)

urlpatterns = patterns('',

        url(r'^lists/', include(list_patterns, namespace='lists')),
        url(r'^talks/', include(talks_patterns, namespace='talks')),

)

