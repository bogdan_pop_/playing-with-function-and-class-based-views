from __future__ import absolute_import
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db.models import Count
from django.views import generic

from django.shortcuts import render, HttpResponseRedirect, get_object_or_404, Http404, redirect


from braces import views

from . import forms
from . import models

from .forms import CreateListForm, TalkForm, TalkRemoveTalkForm
from .models import TalkList, Talk


def talklist_list_view(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('login'))
    obj = TalkList.objects.filter(user=request.user)
    obj = obj.annotate(talk_count=Count('talks'))  # adauga camp aditional cu numele talk_count
    return render(request, "talks/talklist_list.html", {'obj': obj})


def talklist_detail_view(request, slug):
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse("login"))
    obj = TalkList.objects.filter(user=request.user).get(slug=slug)
    form = TalkForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            talk = form.save(commit=False)
            talk.talk_list = obj
            talk.save()
            return redirect(obj)
            # return HttpResponseRedirect(reverse_lazy("talks:lists:list:detail"))
    else:
        form = TalkForm()
    # print form.errors
    return render(request, "talks/talklist_detail.html", {'obj': obj,
                                                          'form': form
                                                         }
                 )


def talklist_create_view(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('login'))
    form = CreateListForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            object = form.save(commit=False)
            object.user = request.user
            object.save()
            messages.success(request, "New list created")
            return HttpResponseRedirect(reverse("talks:lists:list"))
    else:
        form = CreateListForm()
    # print form.errors
    return render(request, "talks/talklist_form.html", {'form': form
                                                        }
                  )


def talklist_update_view(request, slug):
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('login'))
    obj = TalkList.objects.filter(user=request.user).get(slug=slug)
    form = CreateListForm(request.POST or None, instance=obj)
    if request.method == 'POST':
        if form.is_valid():
            object = form.save(commit=False)
            object.save()
            return HttpResponseRedirect(reverse("talks:lists:list"))
    else:
        form = CreateListForm()
    # print form.errors
    return render(request, "talks/talklist_form.html", {'form': form
                                                        }
                  )


def talklist_delete_view(request, pk):  # deletes a Talk list
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('login'))
    obj = get_object_or_404(TalkList, pk=pk)

    if obj.user != request.user:
        raise Http404

    if request.method == 'POST':
        obj.delete()
        return HttpResponseRedirect(reverse("talks:lists:list"))
    return render(request, 'talks/talklist_confirm_delete.html', {'obj': obj})


def talklist_remove_talk(request, pk, talklist_pk):
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('login'))
    talk = get_object_or_404(Talk, pk=pk,
                             talk_list_id=talklist_pk,
                             talk_list__user=request.user)
    # try:
    #     talk = Talk.objects.get(pk=pk,
    #                             talk_list_id=talklist_pk,
    #                             talk_list__user=request.user
    #                             )
    # except models.Talk.DoesNotExist:
    #     raise Http404
    form = TalkRemoveTalkForm(request.POST or None)
    if request.method == 'POST':
        talklist = talk.talk_list
        messages.success(request, u'{} was removed from {}'.format(talk, talklist))
        talk.delete()
        return HttpResponseRedirect(talklist.get_absolute_url())
    return render(request, "talk_confirm_removal.html", {'form': form,
                                                         'talk': talk})


def talklist_schedule_view(request, slug):
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('login'))
    obj = TalkList.objects.filter(user=request.user).get(slug=slug)
    return render(request, "talks/schedule.html", {'obj': obj})


def talk_detail_view(request, slug):
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('login'))
    # try:
        #talk = Talk.objects.filter(talk_list__user=request.user).get(slug=slug)

    # except Talk.DoesNotExist:
    #     raise Http404
    # else:

    talk = get_object_or_404(Talk, talk_list__user=request.user,
                                    slug=slug)

    talk_form = forms.TalkRatingForm(request.POST or None, instance=talk)
    list_form = forms.TalkTalkListForm(request.POST or None, instance=talk)
    if request.method == 'POST':
        if 'save' in request.POST:
            talk_form = forms.TalkRatingForm(request.POST or None, instance=talk)
            if talk_form.is_valid():
                talk_form.save()
                return redirect(talk)
        if 'move' in request.POST:
            list_form = forms.TalkTalkListForm(request.POST or None, instance=talk)
            if list_form.is_valid():
                list_form.save()
                return redirect(talk)
    print talk_form.errors
    return render(request, "talks/talk_detail.html", {'object': talk,
                                                      'rating_form': talk_form,
                                                      'list_form': list_form
                                                      }
                  )


# in these views i overwrote get_queryset twice
# there is a better way...make a mixin

class RestrictToUserMixin(object):
    def get_queryset(self):
        obj = super(RestrictToUserMixin, self).get_queryset()
        obj = obj.filter(user=self.request.user)
        return obj


class TalkListListView(views.LoginRequiredMixin,
                       RestrictToUserMixin,
                       generic.ListView
                       ):
    model = models.TalkList

    def get_queryset(self):
        obj = super(TalkListListView, self).get_queryset()
        obj = obj.annotate(talk_count=Count('talks'))  # adds an atribute talk count to
        return obj  # every record in the queryset and make it be the value of count of the talks
        # that relate to this

    # def get_queryset(self):
    #     return self.request.user.lists.all()


class TalkListDetailView(views.LoginRequiredMixin,
                         views.PrefetchRelatedMixin,
                         RestrictToUserMixin,
                         generic.DetailView
    ):

    form_class = forms.TalkForm  # it doesn't care about this
    http_method_names = ['get', 'post']  # lists the method names the view is allowed to respond to
    model = models.TalkList
    prefetch_related = ('talks',)

    def get_context_data(self, **kwargs):  # method similar to render in function based views
        context = super(TalkListDetailView, self).get_context_data(**kwargs)
        context.update({  # updating the whatever context dictionary has come back with the new form object below
            'form': self.form_class(self.request.POST or None)  # instantiating the form with
        })  # whatever is in request.Post or nothing
        return context

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            obj = self.get_object()  # get the talklist
            talk = form.save(commit=False)  # save the talk
            talk.talk_list = obj  # the talklist on the talk is gonna be the one we just fetched
            talk.save()
        else:
            return self.get(request, *args, **kwargs)
        return redirect(obj)


class TalkListCreateView(views.LoginRequiredMixin,
                         views.SetHeadlineMixin,
                         generic.CreateView):

    form_class = forms.CreateListForm
    headline = 'Create'
    model = models.TalkList

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save()
        return super(TalkListCreateView, self).form_valid(form)


class TalkListUpdateView(RestrictToUserMixin,
                         views.LoginRequiredMixin,
                         views.SetHeadlineMixin,
                         generic.UpdateView
):
    form_class = forms.CreateListForm
    headline = 'Update'
    model = models.TalkList


class TalkListDeleteView(generic.DeleteView):  # deletes the Talk list
    headline = 'Delete'
    model = models.TalkList
    success_url = reverse_lazy("talks:lists:list")
    template_name = "talks/talklist_confirm_delete.html"


class TalkListRemoveTalkView(  # deletes a talk in a talk list
    views.LoginRequiredMixin,
    generic.RedirectView
):
    model = models.Talk

    def get_redirect_url(self, *args, **kwargs):
        return self.talklist.get_absolute_url()

    def get_object(self, pk, talklist_pk):
        try:
            talk = self.model.objects.get(
                pk=pk,
                talk_list_id=talklist_pk,
                talk_list__user=self.request.user
            )
        except models.Talk.DoesNotExist:
            raise Http404
        else:
            return talk

    def get(self, request, *args, **kwargs):
        self.object = self.get_object(kwargs.get('pk'), # gets object out of database
                                      kwargs.get('talklist_pk'))
        self.talklist = self.object.talk_list
        messages.success(
            request,
            u'{0.name} was removed from {1.name}'.format(
               self.object, self.talklist))
        self.object.delete()
        return super(TalkListRemoveTalkView, self).get(request, *args, **kwargs)


class TalkListScheduleView(
    RestrictToUserMixin,
    views.PrefetchRelatedMixin,
    generic.DetailView
) :

    model = models.TalkList
    prefetch_related = ('talks',)
    template_name = 'talks/schedule.html'


class TalkDetailView(generic.DetailView):
    http_method_names = ['get', 'post']
    model = models.Talk

    def get_queryset(self):
        return self.model.objects.filter(talk_list__user=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(TalkDetailView, self).get_context_data(**kwargs)
        obj = context['object']
        list_form = forms.TalkTalkListForm(self.request.POST or None, instance=obj)
        rating_form = forms.TalkRatingForm(self.request.POST or None, instance=obj)

        context.update({
            'rating_form': rating_form,
            'list_form': list_form
        })
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if 'save' in request.POST:
            talk_form = forms.TalkRatingForm(self.request.POST or None, instance=self.object)

            if talk_form.is_valid():
                talk_form.save()
        if 'move' in request.POST:
            list_form = forms.TalkTalkListForm(request.POST or None, instance=self.object)
            if list_form.is_valid():
                list_form.save()
        return redirect(self.object)