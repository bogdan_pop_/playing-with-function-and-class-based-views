from django.contrib.auth.models import User
from django.core.urlresolvers import reverse, reverse_lazy
from django.template.defaultfilters import slugify
from django.test import TestCase
from django.test.client import Client, RequestFactory
from talks.models import TalkList, Talk


class HomeView(TestCase):
    def setUp(self):
        User.objects.create_user(username='B', password='asd')

    def test_to_see_if_on_right_page(self):
        client = Client()
        response = client.get(reverse('home'))
        self.assertContains(response, "Welcome to the Pycon Survival Guide")
        self.assertTrue('Howdy' in response.content)
        self.assertTrue('user' in response.context)
        self.assertTrue(response.context['user'], 'B')


class SignupView(TestCase):
    def setUp(self):
        u = User.objects.create_user(username='B', password='asd')

    def test_to_see_if_on_right_page(self):
        client = Client()
        response = client.get(reverse('signup'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Register for Pycon Survival Guide")
        response = client.post(reverse('signup'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Register for Pycon Survival Guide")

    def test_client_not_logged_in(self):
        client = Client()
        response = client.get(reverse('talks:lists:list'))
        self.assertRedirects(response, reverse('login'))
        response = client.post(reverse("talks:lists:list"))
        self.assertRedirects(response, reverse('login'))

    def test_template_loads(self):
        self.client.login(username='B', password='asd')
        response = self.client.get(reverse('signup'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "accounts/signup.html")

    def test_no_content_to_render(self):
        response = self.client.post(reverse('signup'), {'username': '', 'password1': '', 'password2': ''})
        self.assertFormError(response, 'form', 'username', 'This field is required.')
        self.assertFormError(response, 'form', 'password1', 'This field is required.')
        self.assertFormError(response, 'form', 'password2', 'This field is required.')

    def test_render_invalid_data(self):
        response = self.client.post(reverse('signup'), {'username': 'abc', 'password1': 'asd', 'password2':'abc'})
        self.assertFormError(response, 'form', 'password2', "The two password fields didn't match.")

    def test_corect_content(self):
        response = self.client.post(reverse('signup'), {'username': 'B', 'password1': 'asd', 'password2': 'asd'})
        self.assertFormError(response, 'form', 'username', "A user with that username already exists.")
        response = self.client.post(reverse('signup'), {'username': 'O', 'password1': 'asd', 'password2': 'asd'})
        self.assertRedirects(response, reverse('login'))


class LoginView(TestCase):
    def setUp(self):
        User.objects.create_user(username='B', password='asd')

    def test_login_page(self):
        response = self.client.get(reverse('login'))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('Login' in response.content)
        self.assertTrue('Login to the PyCon Survival Guide' in response.content)

    def test_login_with_no_data(self):
        response = self.client.post(reverse('login'), {'username': '', 'password': ''})
        self.assertContains(response, "This field is required.")
        self.assertFormError(response, 'form', 'username', "This field is required.")
        self.assertFormError(response, 'form', 'password', "This field is required.")

    def test_login_with_incorect_data(self):
        response = self.client.post(reverse('login'), {'username': 'banana', 'password': 'abc'})
        self.assertContains(response, "Invalid.")

    def test_for_successfull_login(self):
        response = self.client.post(reverse('login'), {'username': 'B', 'password': 'asd'})
        self.assertTrue(response.status_code, 302)
        self.assertRedirects(response, reverse('home'))


class TalkListListView(TestCase):
    def setUp(self):
        u = User.objects.create_user(username='B', password='asd')
        TalkList.objects.create(user=u, name='To attend')
        TalkList.objects.create(user=u, name='To not attend')

    def test_list_view_not_logged_in(self):
        response = self.client.get(reverse('talks:lists:list'))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('login'))

    def test_list_view_logged_in(self):
        response = self.client.post(reverse('login'), data={'username':'B', 'password': 'asd'})
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('home'))

    def test_if_talkList_exists(self):
        self.client.login(username='B', password='asd')
        response = self.client.get(reverse('talks:lists:list'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Your Lists')
        self.assertContains(response, 'To attend')
        self.assertTrue('obj' in response.context)
        self.assertTrue(response.context['obj'], 'To attend')
        self.assertTrue(response.context['obj'], 'To not attend')

    def test_is_the_same_talkList(self):
        self.client.login(username='B', password='asd')
        response = self.client.get(reverse('talks:lists:list'))
        self.assertEquals(response.context['obj'].count(), 2)
        self.assertEquals(str(response.context['obj'][0]), str(TalkList.objects.get(pk=1)))
        self.assertEquals(str(response.context['obj'][1]), str(TalkList.objects.get(pk=2)))


class TalkListDetailView(TestCase):

    def setUp(self):
        u = User.objects.create_user(username='B', password='asd')
        t = TalkList.objects.create(user=u, name='To attend')
        Talk.objects.create(talk_list=t, name='Some talk', talk_rating=3, when='2014-04-12 12:30')

    def test_detail_view_not_logged_in(self):
        response = self.client.get(reverse('talks:lists:list'))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('login'))

    def test_detail_view_logged_in(self):
        self.client.login(username='B', password='asd')
        response = self.client.get(reverse('talks:lists:list'))
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('talks:lists:detail', kwargs={'slug': 'to-attend'}))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Some talk')
        self.assertContains(response, 'Add a talk')
        self.assertEqual(str(response.context['obj']), 'To attend')


class TalkListCreateView(TestCase):

    def setUp(self):
        u = User.objects.create_user(username='B', password='asd')

    def test_create_view_not_logged_in(self):
        response = self.client.get(reverse('talks:lists:create'))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('login'))

    def test_view_loads(self):
        self.client.login(username='B', password='asd')
        response = self.client.get(reverse('talks:lists:create'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "talks/talklist_form.html")

    def test_view_with_no_data(self):
        self.client.login(username='B', password='asd')
        response = self.client.post(reverse('talks:lists:create'), {'Name:': ''})
        self.assertEqual(response.status_code, 200)
        self.assertFormError(response, 'form', 'name', 'This field is required.')

    def test_create_view_logged_in(self):
        self.client.login(username='B', password='asd')
        response = self.client.get(reverse('talks:lists:create'))
        self.assertEqual(response.status_code, 200)
        response = self.client.post(reverse('talks:lists:create'), data={'name': 'Not to attend'})
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse("talks:lists:list"))
        # test if new list exists
        response = self.client.get(reverse('talks:lists:list'))
        self.assertEqual(str(response.context['obj']), '[<TalkList: Not to attend>]')


class TalkListUpdateView(TestCase):

    def setUp(self):
        u = User.objects.create_user(username='B', password='asd')
        t = TalkList.objects.create(user=u, name='To attend')

    def test_update_view_not_logged_in(self):
        response = self.client.get(reverse("talks:lists:update", kwargs={'slug': 'to-attend'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('login'))

    def test_update_view_view_loads(self):
        self.client.login(username='B', password='asd')
        response = self.client.get(reverse("talks:lists:update", kwargs={'slug': 'to-attend'}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "talks/talklist_form.html")

    def test_update_view_with_no_data(self):
        self.client.login(username='B', password='asd')
        response = self.client.get(reverse("talks:lists:update", kwargs={'slug': 'to-attend'}))
        self.assertEqual(response.status_code, 200)
        response = self.client.post(reverse("talks:lists:update", kwargs={'slug': 'to-attend'}),
                                   data={'name': ''})
        self.assertEqual(response.status_code, 200)
        self.assertFormError(response, 'form', 'name', "This field is required.")

    def test_update_view_with_good_data(self):
        self.client.login(username='B', password='asd')
        response = self.client.get(reverse("talks:lists:update", kwargs={'slug': 'to-attend'}))
        self.assertEqual(response.status_code, 200)
        response = self.client.post(reverse("talks:lists:update", kwargs={'slug': 'to-attend'}),
                                   data={'name': 'Not to attend'})
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('talks:lists:list'))


class TalkListDeleteView(TestCase):

    def setUp(self):
        u = User.objects.create_user(username='B', password='asd')
        t = TalkList.objects.create(user=u, name='To attend')

    def test_delete_view_not_logged_in(self):
        response = self.client.get(reverse('talks:lists:delete', kwargs={'pk': '0'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('login'))

    def test_delete_view_loads(self):
        self.client.login(username='B', password='asd')
        response = self.client.get(reverse('talks:lists:delete', kwargs={'pk': '1'}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "talks/talklist_confirm_delete.html")
        # print TalkList.objects.filter(user=u, pk=1)

    def test_delete_talklist(self):
        self.client.login(username='B', password='asd')
        response = self.client.get(reverse('talks:lists:delete', kwargs={'pk': '1'}))
        self.assertEqual(str(response.context['obj']), 'To attend')
        self.assertEqual(response.status_code, 200)
        response = self.client.post(reverse('talks:lists:delete', kwargs={'pk':'1'}),
                                    data={'Delete a list': 'confirm'})
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('talks:lists:list'))
        self.assertEqual(TalkList.objects.all().count(), 0)
        # print TalkList.objects.all().count()


class TalkListScheduleView(TestCase):

    def setUp(self):
        u = User.objects.create_user(username='B', password='asd')
        t = TalkList.objects.create(user=u, name='To attend')

    def test_schedule_view_not_logged_in(self):
        response = self.client.get(reverse('talks:lists:schedule', kwargs={'slug': 'to-attend'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('login'))

    def test_schedule_view_loads(self):
        self.client.login(username='B', password='asd')
        response = self.client.get(reverse('talks:lists:schedule', kwargs={'slug': 'to-attend'}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'talks/schedule.html')
        self.assertEqual(str(response.context['obj']), 'To attend')

    def test_schedule_queryset_exists(self):
        self.client.login(username='B', password='asd')
        response = self.client.get(reverse('talks:lists:schedule', kwargs={'slug': 'to-attend'}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(str(response.context['obj']), 'To attend')


class AddingTalkView(TestCase):  # talklist_detail_view

    def setUp(self):
        u = User.objects.create_user(username='B', password='asd')
        t = TalkList.objects.create(user=u, name='To attend')
        t2 = TalkList.objects.create(user=u, name='Not to attend')
        talk = Talk.objects.create(talk_list=t, name='Some talk', host='Me', when='2014-04-12 12:00', room='517D')

    def test_talk_detail_view_not_logged_in(self):
        response = self.client.get(reverse('talks:lists:detail', kwargs={'slug': 'to-attend'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('login'))

    def test_talk_detail_view_queryset_exists(self):
        self.client.login(username='B', password='asd')
        response = self.client.get(reverse('talks:lists:detail', kwargs={'slug': 'to-attend'}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(str(response.context['obj']), 'To attend')
        self.assertEqual(Talk.objects.all().count(), 1)
        self.assertTrue('Some talk' in response.content)
        self.assertEqual(str(Talk.objects.get(host='Me')), 'Some talk')

    def test_creating_new_talk_no_data(self):
        self.client.login(username='B', password='asd')
        response = self.client.post(reverse('talks:lists:detail', kwargs={'slug': 'to-attend'}),
                                    data={'name': '', 'host': '', 'when': '', 'room': ''})
        self.assertEqual(response.status_code, 200)
        self.assertFormError(response, 'form', 'name', 'This field is required.')
        self.assertFormError(response, 'form', 'host', 'This field is required.')
        self.assertFormError(response, 'form', 'when', 'This field is required.')
        self.assertFormError(response, 'form', 'room', 'This field is required.')

    def test_creating_new_talk_with_incorect_data(self):
        self.client.login(username='B', password='asd')
        response = self.client.post(reverse('talks:lists:detail', kwargs={'slug': 'to-attend'}),
                                    data={'name': 'Some other talk', 'host': 'By me',
                                          'when': '2015-03-12 12:30',
                                          'room': 'ABD'})
        self.assertFormError(response, 'form', 'when', "'when' is outside of Pycon")
        self.assertFormError(response, 'form', 'room', "Select a valid choice. ABD is not "
                                                       "one of the available choices.")

    def test_creating_new_talk_wtih_valid_data(self):
        self.client.login(username='B', password='asd')
        response = self.client.post(reverse('talks:lists:detail', kwargs={'slug': 'to-attend'}),
                                    data={'name': 'Some other talk', 'host': 'Me',
                                          'when': '2014-04-12 12:30',
                                          'room': '517D'})
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('talks:lists:detail', kwargs={'slug': 'to-attend'}))
        self.assertEqual(Talk.objects.all().count(), 2)
        self.assertEquals(str(Talk.objects.get(name='Some talk')), 'Some talk')
        self.assertEqual(str(Talk.objects.get(name='Some other talk')), 'Some other talk')

    def test_check_newly_created_talk(self):
        self.client.login(username='B', password='asd')
        response = self.client.post(reverse('talks:lists:detail', kwargs={'slug': 'to-attend'}),
                                    data={'name': 'Some fucking talk', 'host': 'Me',
                                          'when': '2014-04-12 12:30',
                                          'room': '517D'})
        # intru pe talk
        response = self.client.get(reverse('talks:talks:detail', kwargs={'slug': 'some-fucking-talk'}))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Me')
        self.assertContains(response, 'Some fucking talk')

        self.assertEqual(str(response.context['object']), 'Some fucking talk')

    def test_moving_a_talk(self):
        self.client.login(username='B', password='asd')
        response = self.client.post(reverse('talks:lists:list'), data={'Create a new list': 'Not to attend'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(TalkList.objects.all().count(), 2)
        self.assertEqual(str(TalkList.objects.get(name='Not to attend')), 'Not to attend')
        response = self.client.get(reverse('talks:lists:detail', kwargs={'slug': 'to-attend'}))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Some talk')
        response = self.client.post(reverse('talks:talks:detail', kwargs={'slug': 'some-talk'}),
                                    data={'move': 'not-to-attend'})
        self.assertEqual(response.status_code, 200)
        response = self.client.get(reverse('talks:talks:detail', kwargs={'slug': 'some-talk'}))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, Talk.objects.get(name='Some talk'))

    def test_saving_rating_to_a_talk(self):
        self.client.login(username='B', password='asd')
    #     create another talklist
        self.assertEqual(TalkList.objects.all().count(), 2)
        response = self.client.post(reverse('talks:talks:detail', kwargs={'slug': 'some-talk'}),
                         data={'talk_rating': '3', 'speaker_rating': '4', 'save': 'submit'})
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('talks:talks:detail', kwargs={'slug': 'some-talk'}))


class RemovingTalk(TestCase):

    def setUp(self):
        u = User.objects.create_user(username='B', password='asd')
        t = TalkList.objects.create(user=u, name='To attend')
        talk = Talk.objects.create(talk_list=t, name='Some talk', host='Me', when='2014-04-12 12:00', room='517D')

    def test_remove_talk_view_not_logged_in(self):
        response = self.client.get(reverse('talks:lists:remove_talk', kwargs={
            'talklist_pk': '1', 'pk': '1'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('login'))

    def test_remove_talk_view_template(self):
        self.client.login(username='B', password='asd')
        response = self.client.get(reverse('talks:lists:remove_talk', kwargs={
            'talklist_pk': '1', 'pk': '1'}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'talk_confirm_removal.html')

    def test_remove_talk_view(self):
        self.client.login(username='B', password='asd')
        # making sure there is 1 talk obj
        # and that it's the right talk obj
        response = self.client.get(reverse('talks:lists:remove_talk', kwargs={
            'talklist_pk': '1', 'pk': '1'}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Talk.objects.all().count(), 1)
        self.assertEqual(str(response.context['talk']), 'Some talk')
        # removing talk obj
        response = self.client.post(reverse('talks:lists:remove_talk', kwargs={
            'talklist_pk': '1', 'pk': '1'}), data={'remove a talk': 'confirm'})
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('talks:lists:detail', kwargs={'slug': 'to-attend'}))
        self.assertEqual(Talk.objects.all().count(), 0)
