from django.contrib.auth.models import User
from django.core.urlresolvers import reverse, reverse_lazy
from django.template.defaultfilters import slugify
from django.test import TestCase
from django.test.client import Client, RequestFactory
from talks.models import TalkList, Talk


class TalkListModels(TestCase):
    def setUp(self):
        u = User.objects.create(username='B')
        t = TalkList.objects.create(user=u, name='To attend')

    def test_sanity_check(self):
        u = User.objects.all()
        t = TalkList.objects.all()
        self.assertTrue(u.count(), 1)
        self.assertTrue(t.count(), 1)

    def test_TalkList_username(self):
        u = User(username='B')
        t = TalkList(user=u, name='To attend')
        self.assertEqual(str(t.user), 'B')

    def test_TalkList_listname(self):
        u = User(username='B')
        t = TalkList(user=u, name='To attend')
        self.assertEqual(t.name, 'To attend')

    def test_TalkList_slug(self):
        u = User(username='B')
        t = TalkList(user=u, name='To attend')
        t.slug = slugify(t.name)
        self.assertEqual(t.slug, 'to-attend')
