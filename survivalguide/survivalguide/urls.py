from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

from . import views
from .views import LogoutView


urlpatterns = patterns('',

    url(r'^$', views.home, name='home'),  # home view as function
    # url(r'^$', Home.as_view(), name='home'),  # home view as a class

    url(r'^accounts/register/$', views.sign_up_view, name='signup'),
    # url(r'accounts/register/$', views.SignUpView.as_view(), name='signup'),

    url(r'^accounts/login/$', views.login_view, name='login'),
    # url(r'^accounts/login/$', LoginView.as_view(), name='login'),

    url(r'^accounts/logout/$', views.logout_view, name='logout'),
    # url(r'^accounts/logout/$', LogoutView.as_view(), name='logout'),


    url(r'talks/', include('talks.urls', namespace='talks')),

    url(r'^admin/', include(admin.site.urls))
)