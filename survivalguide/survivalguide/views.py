from __future__ import absolute_import
from django.core.urlresolvers import reverse_lazy, reverse
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import generic

from .forms import RegistrationForm, UserCreateForm, LoginForm, LoginForm2, AuthenticationForm, UserCreationForm

from braces import views

from talks.models import TalkList


def home(request):
    user = request.user
    return render(request, "home.html", {'user': user})


def sign_up_view(request):
    form = UserCreateForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            user = form.save()
            messages.success(request, "Bravo, te-ai inregistrat")
            TalkList.objects.create(user=user, name='To attend')
            return HttpResponseRedirect(reverse_lazy("login"))
    # print form.errors
    return render(request, "accounts/signup.html", {'form': form})


def login_view(request):
    form = LoginForm2(request.POST or None)
    error = None
    if request.method == 'POST':
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)

            if user is not None and user.is_active:
                login(request, user)
                messages.success(request, "Te-ai logat")
                return HttpResponseRedirect(reverse_lazy("home"))

            error = 'Invalid.'
    # print form.errors
    return render(request, "accounts/login.html", {'form': form, 'error': error})


def logout_view(request):
    logout(request)
    messages.success(request, "Bye!!")
    return HttpResponseRedirect(reverse("home"))


class Home(generic.TemplateView):
    template_name = "home.html"


class SignUpView(generic.CreateView):
    form_class = UserCreateForm
    form_valid_message = "Thanks for registering. You can login now."
    model = User
    success_url = reverse_lazy("login")
    template_name = "accounts/signup.html"

    def form_valid(self, form):
        resp = super(SignUpView, self).form_valid(form)
        TalkList.objects.create(user=self.object, name='To attend')
        return resp


class LoginView(generic.FormView):
    form_class = LoginForm2
    success_url = reverse_lazy('home')
    template_name = 'signup.html'

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                login(self.request, user)
                return super(LoginView, self).form_valid(form)
        else:
            return self.form_invalid(form)


class LogoutView(generic.RedirectView):
    url = reverse_lazy("home")

    def get(self, request, *args, **kwargs):
        logout(request)
        messages.success(self.request, "Bye!!!")
        return super(LogoutView, self).get(request, *args, **kwargs)

